// Container for contact list
var ContactBox = React.createClass({
	// API request to get all contacts 
	loadContactsFromServer: function() {
		$.ajax({
			url: this.props.url,
			dataType: 'json',
			cache: false,
			success: function(data) {
				this.setState({data: data});
				this.setState({all_contacts: data});
			}.bind(this),
			error: function(xhr, status, err) {
				console.error(this.props.url, status, err.toString());
			}.bind(this)
		});
	},
	// Updates local contacts array and posts new contact to API and hides new contact form
	handleContactSubmit: function(contact) {
		var contacts = this.state.all_contacts;
		var newContacts = contacts.concat([contact]);
		this.setState({data: newContacts});
		this.setState({all_contacts: newContacts});
		$.ajax({
			url: this.props.url,
			dataType: 'json',
			type: 'POST',
			data: contact,
			success: function(data) {
				this.setState({data: data});
			}.bind(this),
			error: function(xhr, status, err) {
				this.setState({data: contacts});
				console.error(this.props.url, status, err.toString());
			}.bind(this)
		});
		this.hideForm();
	},
	// Filters out contacts that don't make the search term
	handleSearch: function(e) {
		var contacts = this.state.all_contacts;
		var term = e.target.value;
		var filteredContacts = contacts.filter(function(contact) {
            return (contact.first_name + ' ' + contact.last_name).toLowerCase().indexOf(term.toLowerCase()) > -1;
        });
		this.setState({data: filteredContacts});
	},
	// Blanks out search field and re-displays all contacts
	clearSearch: function(e) {
		var contacts = this.state.all_contacts;
		this.setState({data: contacts});
		$('#search-bar').val('');
	},
	getInitialState: function() {
		return {data: []};
	},
	// Load contact data once mounted
	componentDidMount: function() {
		this.loadContactsFromServer();
	},
	// Shows new contact form
	showForm: function(e) {
		e.preventDefault();
		$('#contact-form-container').slideDown();
	},
	// Hides new contact form
	hideForm: function() {
		$('#contact-form-container').slideUp();
	},
	// Renders class html
	render: function() {
		return (
			<div className="panel panel-primary">
				<div className="panel-heading">
					<h1>Contacts</h1>
				</div>
				<div className="panel-body">
					<div id="search-form-container" className="panel panel-success">
						<div className="panel-body">
							<form>
								<div className="row">
									<div className="col-md-3">
										<div className="input-group">
											<input id="search-bar" type="text" className="form-control" placeholder="Search" 
												onChange={this.handleSearch} />
											<span className="input-group-btn">
												<button type="button" className="btn btn-danger" onClick={this.clearSearch}>X</button>
											</span>
										</div>
									</div>
									<div className="col-md-2 pull-right">
										<button type="button" className="btn btn-success" onClick={this.showForm}>Add Contact</button>
									</div>
								</div>
							</form>
							<div className="well" id="contact-form-container" style={{display: "none"}}>
								<ContactForm onContactSubmit={this.handleContactSubmit} />
							</div>
						</div>
					</div>
					<ContactList data={this.state.data} />
				</div>
			</div>
		);
	}
});

// Just a list of Contacts
var ContactList = React.createClass({
	// Renders class html
	render: function() {
		// Pipe contact data into the Contact class
		var contactNodes = this.props.data.map(function(contact) {
			return (
				<Contact key={contact.id} first_name={contact.first_name} last_name={contact.last_name} 
					home_phone={contact.home_phone} mobile_phone={contact.mobile_phone} work_phone={contact.work_phone}
					email={contact.email} alt_email={contact.alt_email} />
			);
		}, this);
		return (
			<div id="contact-list-container" className="row">
				{contactNodes}
			</div>
		);
	}
});

// Displays contact info
var Contact = React.createClass({
	// Renders class html
	render: function() {
		// Only displays fields we have values for
		var home_phone, mobile_phone, work_phone, email, alt_email;
		if (this.props.home_phone) {
			home_phone = <div className="col-md-2"><strong>Home:</strong> {this.props.home_phone}</div>;
		}
		if (this.props.mobile_phone) {
			mobile_phone = <div className="col-md-2"><strong>Mobile:</strong> {this.props.mobile_phone}</div>;
		}
		if (this.props.work_phone) {
			work_phone = <div className="col-md-2"><strong>Work:</strong> {this.props.work_phone}</div>;
		}
		if (this.props.email) {
			email = <div className="col-md-3"><strong>Email:</strong> {this.props.email}</div>;
		}
		if (this.props.alt_email) {
			alt_email = <div className="col-md-3"><strong>Alt Email:</strong> {this.props.alt_email}</div>;
		}
		return (
			<div className="panel panel-info">
				<div className="panel-heading">
					<h3 className="panel-title">{this.props.first_name} {this.props.last_name}</h3>
				</div>
				<div className="panel-body">
					{home_phone}
					{mobile_phone}
					{work_phone}
					{email}
					{alt_email}
				</div>
			</div>
		);
	}
});

// Form for adding new contacts
var ContactForm = React.createClass({
	// Initialize a blank form
	getInitialState: function() {
		return {
			first_name: "",
			last_name: "", 
			home_phone: "",
			mobile_phone: "",
			work_phone: "",
			email: "",
			alt_email: ""
		};
	},
	// Updates state as form is filled out
	handleChange: function(e) {
		var targetId = e.target.id;
		var changeJSON = {};
		changeJSON[targetId] = e.target.value;
		this.setState(changeJSON);
	},
	// Sends new contact info back up to ContentBox to be posted to API
	handleSubmit: function(e) {
		e.preventDefault();
		var first_name = this.state.first_name;
		var last_name = this.state.last_name;
		var home_phone = this.state.home_phone;
		var mobile_phone = this.state.mobile_phone;
		var work_phone = this.state.work_phone;
		var email = this.state.email;
		var alt_email = this.state.alt_email;
		if (!first_name || !last_name) {
		  return;
		}
		this.props.onContactSubmit({
			first_name: first_name, 
			last_name: last_name, 
			home_phone: home_phone, 
			mobile_phone: mobile_phone, 
			work_phone: work_phone, 
			email: email, 
			alt_email: alt_email
		});
		// Blank out form after contact info is sent
		this.setState({
			first_name: "",
			last_name: "", 
			home_phone: "",
			mobile_phone: "",
			work_phone: "",
			email: "",
			alt_email: ""
		});
	},
	// Renders class html
	render: function() {
		return (
			<form onSubmit={this.handleSubmit}>
				<div className="row">
					<div className="col-md-2 col-md-offset-3">
						<div className="form-group">
							<label htmlFor="first_name">First Name</label>
							<input type="text" className="form-control" id="first_name" placeholder="John" 
								required value={this.state.first_name} onChange={this.handleChange} />
						</div>
					</div>
					<div className="col-md-2">
						<div className="form-group">
							<label htmlFor="last_name">Last Name</label>
							<input type="text" className="form-control" id="last_name" placeholder="Smith" 
								required value={this.state.last_name} onChange={this.handleChange} />
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-md-2 col-md-offset-3">
						<div className="form-group">
							<label htmlFor="home_phone">Home Phone</label>
							<input type="tel" className="form-control" id="home_phone" placeholder="555-555-5555" 
								value={this.state.home_phone} onChange={this.handleChange} />
						</div>
					</div>
					<div className="col-md-2">
						<div className="form-group">
							<label htmlFor="mobile_phone">Mobile Phone</label>
							<input type="tel" className="form-control" id="mobile_phone" placeholder="555-555-5555" 
								value={this.state.mobile_phone} onChange={this.handleChange} />
						</div>
					</div>
					<div className="col-md-2">
						<div className="form-group">
							<label htmlFor="work_phone">Work Phone</label>
							<input type="tel" className="form-control" id="work_phone" placeholder="555-555-5555" 
								value={this.state.work_phone} onChange={this.handleChange} />
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-md-2 col-md-offset-3">
						<div className="form-group">
							<label htmlFor="email">Email Address</label>
							<input type="email" className="form-control" id="email" placeholder="email@example.com" 
								value={this.state.email} onChange={this.handleChange} />
						</div>
					</div>
					<div className="col-md-2">
						<div className="form-group">
							<label htmlFor="alt_email">Alternative Email</label>
							<input type="email" className="form-control" id="alt_email" placeholder="email@example.com" 
								value={this.state.alt_email} onChange={this.handleChange} />
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-md-1 col-md-offset-8">
						<button id="submit_save" className="btn btn-primary" type="submit" >Save</button>
					</div>
				</div>
			</form>
		);
	}
});

// Renders all this html and injects it into the content div on main page
ReactDOM.render(
	<ContactBox url="/contacts" />,
	document.getElementById('content')
);
