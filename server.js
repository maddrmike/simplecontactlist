var fs = require('fs'),
	path = require('path'),
	express = require('express'),
	async = require('async'),
	cassandra = require('cassandra-driver'),
	bodyParser = require('body-parser'),
	app = express();

// Connect to Cassandra 
var client = new cassandra.Client( { contactPoints : [ 'localhost:9042' ] } );
client.connect(function(err, result) {
    console.log('Connected.');
});

// Listen on port 3000
app.set('port', (process.env.PORT || 3000));

// Set contents of public dir to be served statically 
app.use('/', express.static(path.join(__dirname, 'public')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Cache-Control', 'no-cache');
    next();
});

// API request to get all contacts
app.get('/contacts', function (req, res) {
    client.execute('SELECT * FROM contact_list.contacts', function(err, results) {
		if (err) throw err;
		res.json(results['rows']);
	});
});

// API post to add a new contact
app.post('/contacts', function (req, res) {
    var id = cassandra.types.uuid();
	client.execute('INSERT INTO contact_list.contacts (id, first_name, last_name, home_phone, mobile_phone, work_phone, email, alt_email) VALUES(?, ?, ?, ?, ?, ?, ?, ?);', 
		[id, req.body.first_name, req.body.last_name, req.body.home_phone, req.body.mobile_phone, req.body.work_phone, req.body.email, req.body.alt_email], 
		function(err, results) {
			if (err) throw err;
			res.json(results);
		}
	);
});

// Start up server listening on http://localhost:3000
app.listen(app.get('port'), function() {
  console.log('Server started: http://localhost:' + app.get('port') + '/');
});
