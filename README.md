# Simple Contact List

This is as simple contact list app that lets you add and search contacts using a React and Bootstrap frontend that utilizes an Express rest API that saves the contacts in a simple Cassandra database.

The file keyspace_and_table_creation.cql contains a script to create the keyspace and table used by the app and pre-populates it with a few contacts.

Screenshots of the app are in the screenshots directory.

To install, run keyspace_and_table_creation.cql and update 'localhost:9042' on line 10 of server.js to your cassandra database address, then

	'''
	npm install
	'''
	
	'''
	node server.js
	'''
	
and navigate to http://localhost:3000/
